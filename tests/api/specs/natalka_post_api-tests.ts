import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { access } from "fs";
import { PostController } from "../lib/controllers/post.contoller";

const users = new UsersController();
const auth = new AuthController();
const post = new PostController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const email="123natali.s@gmail.com";
const pass="12qwerty2";

describe(`Users controller`, () => {

    let userId: number;
    let accessToken: string;

    before (`Login user`, async () => {
        let response = await auth.login(email, pass);
        checkStatusCode(response,200);
        checkResponseTime (response,1000);
        expect(response.body.user.email).to.be.equal(email, "Email is correct");
        accessToken = response.body.token.accessToken.token;
    });    
 
    it(`should create new post`, async () => {
        var object = {
            authorId: 499,
            previewImage: "string",
            body: "This is a new post",
        };

        let response = await post.createNewPost(object, accessToken);
        checkStatusCode(response,200);
        checkResponseTime (response,1000);
        //console.log("Nata createNewPost");
        //console.log(response.body);

    });
    

    it(`should add reaction to the post`, async () => { 
      
        var object1 = {
            entityId: 693,
            isLike: true,
            userId: 499,
        };

        let responseAddReactionToPost = await post.addReactionToPost(object1, accessToken);
        checkStatusCode(responseAddReactionToPost,200);
        checkResponseTime (responseAddReactionToPost,1000);
        //console.log("Nata responseAddReactionToPost");
        //console.log(responseAddReactionToPost.body);
        
    });


    it(`should add comment to the post`, async () => { 
      
        var object2 = {
            authorId: 499,
            postId: 693,
            body: "New comment for post",
        };

        let responceAddCommentToPost = await post.addCommentToPost(object2, accessToken);
        checkStatusCode(responceAddCommentToPost,200);
        checkResponseTime (responceAddCommentToPost,1000);
        //console.log("Nata responceAddCommentToPost");
        
    });

    it(`should return all posts`, async () => { 
      
        let responseGetAllPosts = await post.getAllPosts();
        checkStatusCode(responseGetAllPosts,200);
        checkResponseTime (responseGetAllPosts,1000);
        //console.log("Nata responseGetAllPosts");
        var allPosts=responseGetAllPosts.body;
        for(var i=0;i<allPosts.length;i++){
            var postS = allPosts[i];
            if(postS.id == 693){
                //console.log(postS);
                //console.log(postS.comments[0].author);
            }
        }
        
    });

    it(`should add the second comment for the post`, async () => { 
      
        var object3 = {
            authorId: 499,
            postId: 693,
            body: "The second comment for post",
        };

        let responceAddCommentToPost = await post.addCommentToPost(object3, accessToken);
        checkStatusCode(responceAddCommentToPost,200);
        checkResponseTime (responceAddCommentToPost,1000);
        //console.log("Nata responceAddCommentToPost2")
        //console.log(responceAddCommentToPost.body);
        
    });
        
    it(`should return the list of all posts`, async () => { 
      
        let responseGetAllPosts = await post.getAllPosts();
        checkStatusCode(responseGetAllPosts, 200);
        checkResponseTime(responseGetAllPosts,1000);
        
    });
       
 });

   