import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { access } from "fs";
import { register } from "ts-node";
import { clear } from "console";

const users = new UsersController();
const auth = new AuthController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

const email="natali2.s@gmail.com";
const pass="12qwerty";

describe(`Users controller`, () => {

    let userId: number;
    let accessToken: string;

    let userData={
        id: 0,
        avatar: "NataAva",
        email: email,
        userName: "Nata",
        password: pass
      };

    before(`Create user`, async () => {
        
        let responseNewUser = await users.registerNewUser(userData);
        userId = responseNewUser.body.user.id;
        checkStatusCode(responseNewUser,201)
        checkResponseTime(responseNewUser,1000);

        expect(responseNewUser.body.user.email).to.be.equal(email, "Email is correct");
        expect(responseNewUser.body.user.userName == "R2D2").to.be.false;
        expect(responseNewUser.body.user.id == 0).to.be.false;
      
    });

    it(`should return all users`, async () => {
        let responseGetUser = await users.getAllUsers();
        checkStatusCode(responseGetUser, 200);
        checkResponseTime(responseGetUser,1000);
    });
    

    it(`Login user`, async () => {
        let response = await auth.login(email, pass);
        checkStatusCode(response,200);
        checkResponseTime (response,1000);
        expect(response.body.user.email).to.be.equal(email, "Email is correct");
        accessToken = response.body.token.accessToken.token;
    });    

   
    it(`should return current user by token`, async () => {
        let responseCurrentUser = await users.getCurrentUser(accessToken);
        //console.log(responseCurrentUser.body);
        checkStatusCode(responseCurrentUser, 200);
        checkResponseTime(responseCurrentUser,1000);
    });

    it(`updateUser`, async () => {
        let responseCurrentUser = await users.getCurrentUser(accessToken);
        checkStatusCode(responseCurrentUser, 200);
        checkResponseTime(responseCurrentUser,1000);

        userData = responseCurrentUser.body;
        userData.userName = "R2D2";

        let responseUpdateUser = await users.updateUser(userData, accessToken);
        checkStatusCode(responseUpdateUser, 204);
        checkResponseTime(responseUpdateUser,1000);

        let responseCurrentUserUpdated = await users.getCurrentUser(accessToken);
        checkStatusCode(responseCurrentUserUpdated, 200);
        checkResponseTime(responseCurrentUserUpdated,1000);
        userData = responseCurrentUserUpdated.body;
        expect(userData.userName).to.be.equal("R2D2", "User name is changed");
        
        expect(userData.userName == "Nata").to.be.false;
        expect(responseCurrentUserUpdated.statusCode == 201).to.be.false;

    });

    it(`should return correct details for current user`, async () => {
       
        let responseCurrentUser = await users.getCurrentUser(accessToken);
        checkStatusCode(responseCurrentUser, 200);
        checkResponseTime(responseCurrentUser,1000);
       
    });

    it(`should return correct details for user by id`, async () => {
       
        let responseGetUserById = await users.getUserById(userId);
        checkStatusCode(responseGetUserById, 200);
        checkResponseTime(responseGetUserById,1000);
       
    });

    after(`Delete user`, async () => {
        var responseDeleteUserById = await users.deleteUserById(userId, accessToken);
        checkStatusCode(responseDeleteUserById, 204);
        checkResponseTime(responseDeleteUserById,1000);
    });

    });

   