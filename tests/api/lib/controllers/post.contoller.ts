import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostController {
    
    async createNewPost(postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .bearerToken(accessToken)
            .body(postData)
            .send();
        return response;
    }

    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async addReactionToPost(reaction: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .bearerToken(accessToken)
            .body(reaction)
            .send();
        return response;
    }

    async addCommentToPost(comment: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(comment)
            .bearerToken(accessToken)
            .send();
        return response;
    }

}