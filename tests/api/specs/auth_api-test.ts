import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostController } from "../lib/controllers/post.contoller";


const users = new UsersController();
const auth = new AuthController();
const post = new PostController();

xdescribe("Token usage", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("natali.s@gmail.com", "12qwerty2");
 
        accessToken = response.body.token.accessToken.token;
    
    });

    it(`Usage is here`, async () => {
        let userData: object = {
            id: 499,
            avatar: "string",
            email: "natali.s@gmail.com",
            userName: "natali",
        };

        let response = await users.updateUser(userData, accessToken);
        expect(response.statusCode, `Status Code should be 204`).to.be.equal(204);
    });
    
    it(`Usage is here`, async () => {
        let postData: object = {
            authorId: 500,
            previewImage: "string",
            body: "This is a new post",
        };

        let response = await post.createNewPost(postData, accessToken);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    });

    it(`Usage is here`, async () => {
        let reaction: object = {
            entityId: 693,
            isLike: true,
            userId: 499,
        };

        let response = await post.addReactionToPost(reaction, accessToken);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    });

    it(`Usage is here`, async () => {
        let comment: object = {
            authorId: 499,
            postId: 693,
            body: "New comment for post",
        };

        let response = await post.addCommentToPost(comment, accessToken);
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    });
});
